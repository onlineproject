class CreateStages < ActiveRecord::Migration
  def self.up
    create_table :stages do |t| 
      t.date      :date
      t.text      :question
     
      t.text      :feed_useraction
      t.text      :feed_selfaction
      t.text      :feedtext1
      t.text      :feedtext2

      
      
      t.string    :feedpic_file_name
      t.string    :feedpic_content_type
      t.integer   :feedpic_file_size

    end
    execute %{ALTER TABLE users CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin}
  end

 def self.down
    drop_table :stages
  end
end
