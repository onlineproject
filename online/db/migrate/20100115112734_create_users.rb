class CreateUsers < ActiveRecord::Migration
  def self.up
    create_table :users do |t|
      t.string :uid
      t.string :name
      t.string :usermail
      t.date   :birthday
      t.text   :status
      t.string :relation
      t.string :gender
      t.string :first_name
      t.string :last_name
      t.string :hometown_location
      t.string :networking
      t.string :current_location      
      t.boolean:deleted, :default => false
      t.string :affiliations
      t.integer:friends_invited, :default => 0
      t.integer:friends_published, :default => 0
      t.date   :last_right_answer
  
      t.timestamps
    end
    execute %{ALTER TABLE users CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin}

  end

  def self.down
    drop_table :fbusers
  end
end
