class CreateStatics < ActiveRecord::Migration
  def self.up
    create_table :statics do |t|
      t.string :product_tab_name
      t.text :product_description
      t.string :productpic_file_name
      t.string :productpic_content_type
      t.integer :productpic_file_size
      t.string :bannerpic_file_name
      t.string :bannerpic_content_type
      t.integer :bannerpic_file_size
      t.string :banner_link
      t.string :greetings

      t.timestamps
    end
  end

  def self.down
    drop_table :statics
  end
end
