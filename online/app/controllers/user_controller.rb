require 'timeout'   

class UserController < ApplicationController

  before_filter :source_statistic 
  ensure_application_is_installed_by_facebook_user
  before_filter :loading_static  
  Feed_action = "Play now"

  def source_statistic
    @time_counter =  Time.now
    @time_counter0 =  Time.now

    if (params[:f]) && (params[:f].to_i != 0)
      add_stat(0, VISITED, params[:f] ? params[:f].to_i : 0)
    end         
  end
  
  def loading_static    
    @statics = Static.find(:first)
    if @statics == nil
      render :facebook_error
    end
  end

  def processing_phraze (text_to_process, owner_name, friends_names,gender = "",number_of_friends = 0)
    text = text_to_process
    text = text.gsub("{owner_name}",owner_name)
    text = text.gsub("{friends_names}",friends_names)
    text = text.gsub("{number_of_friends}",number_of_friends.to_s)
    text = text.gsub("{his/her}", gender == "female" ? "her": "his")
    text = text.gsub("{he/she}",gender == "female" ? "she": "he")
    text = text.gsub("{him/her}",gender == "female" ? "her": "him")

    return text 
    #text = text.gsub("{friends_names}",friends_names)
  end 
  def permission
    @userF = session[:facebook_session].user
    @fbuser = User.find(:first, :conditions => ["uid = ?", @userF.uid])
    if @fbuser.blank? || @fbuser.deleted
      begin
        timeout 5 do
          @user_has_permission = @userF.has_permission?("publish_stream") 
        end
      rescue TimeoutError 
        Rails.logger.warn " >" * 15
        Rails.logger.warn "     TIMEOUT WHEN WAS ASKIN FOR PERMISSION"
        Rails.logger.warn " >" * 15  
        render :action => :facebook_error
        return
      end

     if @user_has_permission
        @fbuser = User.find(:first, :conditions => ["uid = ?", @userF.uid])
        
        if @fbuser && @fbuser.deleted
          debug_message("User #{session[:facebook_session].user.uid} was already exist but he was in deleted status. So now he is totally deleted and will be added soon")
          @fbuser.destroy
          @fbuser = nil
        end
        #if user does not exist yet in DB inserting in DB
        if @fbuser == nil
          #getting user info
          begin
            #if user has  birthday
            if @userF.birthday != nil
              birthday_mask = /(\w+)\ (\d+)\, (\d+)/
              dateOfBirth = birthday_mask.match(@userF.birthday)
              #if birthday ok for us
              if (Months[$1] == nil)
                birthday = ""
              else
                birthday = Date.new(dateOfBirth[3].to_i,Months[dateOfBirth[1]],dateOfBirth[2].to_i)
              end
            else
              birthday = ""
            #end if @userF.birthday != nil
            end
            #gathering  affiliations
            @affiliation  = ""
            if @userF.affiliations
              @userF.affiliations.each do |one_affiliation|
                @affiliation += one_affiliation.name + " | "
              end
            end
            
            #gathering other params
            myParams = {
                          :uid               => @userF.uid,
                          :birthday          => birthday,
                          :relation          => @userF.relationship_status,
                          :gender            => @userF.sex,
                          :first_name        => @userF.first_name,
                          :last_name         => @userF.last_name,
                          :status            => "WAS NOT SET YET" ,
                          :hometown_location => (@userF.hometown_location != nil) ? @userF.hometown_location.country.to_s + "/"+  @userF.hometown_location.state.to_s + "/" + @userF.hometown_location.city.to_s : "",
                          :affiliations      => @affiliation,
                          :usermail          => @userF.email
                      
                        }
          rescue
            debug_message("Cannot get information about #{session[:facebook_session].user.uid}",false)
            render :facebook_error
          else
            #saving user into DB
            @fbuser = User.new(myParams)
            debug_message("User #{session[:facebook_session].user.uid} was added to DB")
            @fbuser.save
            add_stat(@userF.uid, PERMISSION)
           
          #end of getting user info
          end
        #end if @fbuser == nil
        end 
        today_question_select 0
        @question_number = "question1"
        render :action => @question_number    
      #if @userF.has_permission?("publish_stream")
      end
    else
      today_question_select 0
      @question_number = "question1"
      render :action => @question_number 
    #if user not in DB
    end
  end
  
  def today_question_select number
    today_now = Time.now  
    today = Date.new(today_now.year,today_now.month,today_now.day)
    today_questions  = Stage.find(:all, :conditions => ["date = ?",  today ] ) 
    @today_question = today_questions[number]
    if @today_question.blank?
      today_questions  = Stage.find(:all, :order => "date DESC"  )
      @today_question = today_questions[number]
      if @today_question.blank?
        render :facebook_error
      end
    end 
  end
 
  def question1
    today_question_select 0
    user_has_published_once = false
    user_has_published_once = question_processor
    if user_has_published_once == true
      @next_step = "question2"
      render :action => "greetings"  
    end
  end
 
  def question2
    today_question_select 1
    user_has_published_once = false
    user_has_published_once = question_processor
    if user_has_published_once == true
      @next_step = "question3"
      render :action => "greetings"  
    end
  end

  def question3

    today_question_select 2
    user_has_published_once = false
    user_has_published_once = question_processor
    if user_has_published_once == true
      @next_step = "question1"
      render :action => "greetings" 
    end
  end

  def question_processor
    time_counter ("before of question_processor time was tooken")

    user_has_published_once = false
    users_array = []
    session_new = Facebooker::Session.create
    for i in 0...6
      if params["friend_sel0#{i}"] != nil && params["friend_sel0#{i}"] != ""    
        @userFB2 =Facebooker::User.new(params["friend_sel0#{i}"], session_new)
        time_counter ("Creating one user")
        users_array << @userFB2 
        user_has_published_once = true
      #end of if
      end
    #end of for
    end 
    
    if user_has_published_once == true

      begin
        #raise TimeoutError
        timeout 7 do
          session_new.batch do 
            users_array.each do |player| 
              player.first_name
            end 
          end 
        end
      rescue TimeoutError 
        Rails.logger.warn " >" * 15
        Rails.logger.warn "     TIMEOUT FOR GETTING NAMES "
        Rails.logger.warn " >" * 15
        render :action => :facebook_error
        return
      end

      time_counter ("Batching first names")
  
      users_array.each do |player|
        p player.first_name 
      end
      time_counter ("Showing result")
      feeds_to_publish = [] 

      @userFB = Facebooker::User.new(session[:facebook_session].user.uid, session_new)
      time_counter ("Creating self user")
      @user_local = User.find(:first, :conditions => ["uid = ?", session[:facebook_session].user.uid])
      #@userFB.sex = session[:facebook_session].user.sex
      time_counter ("Setting sex")
      user_names = []
      #for third time
      self_publishing = ""
      scenario_themself = ""
      #preparing all names into array
      
      for i in 0..(users_array.size - 1)
        user_names << users_array[i].first_name
      end 
   
      time_counter ("Getting first_name")
      #creating messages for list of friends
      for i in 0..(users_array.size - 1)  
        themessage = ""
        user_names_new = user_names - [users_array[i].first_name]
        if user_names_new.size == 0
          themessage = ""
        elsif user_names_new.size == 1
          themessage = "#{user_names_new[0]}"
        elsif user_names_new.size == 2
          themessage = "#{user_names_new[0]} and #{user_names_new[1]}"
        elsif user_names_new.size == 3
          themessage = "#{user_names_new[0]}, #{user_names_new[1]} and #{user_names_new[2]}"
        elsif user_names_new.size == 4
          themessage = "#{user_names_new[0]}, #{user_names_new[1]},#{user_names_new[2]} and #{user_names_new[3]}"
        end
        #preparing for the third stage
        if themessage != ""
          self_publishing = users_array[i].first_name + ( users_array.size > 2 ? ", ":" and ") + themessage
        else
          self_publishing = users_array[i].first_name + ""
        end
        if themessage != ""
          themessage += ""
        end
        session[:self_publishing] = self_publishing
        #publishing
         
        scenario_friend = processing_phraze(@today_question.feed_useraction, users_array[i].first_name, themessage,users_array[i].sex,user_names.size())
        scenario_themself = processing_phraze(@today_question.feed_selfaction, @user_local.first_name, self_publishing,@user_local.gender,user_names.size())

        debug_message scenario_friend
        new_params = Hash.new(0) 
        new_params = { 
                :user_from   => @userFB,
                :user_to   => users_array[i],
                :main_message => scenario_friend
                }
        feeds_to_publish.push new_params
        time_counter ("Was added to hash")
        
      #end of for i in 0..users_array.size
      end
      
      @user_local.friends_published += (user_names.size + 1)
      @user_local.save

      add_stat(0, PUBLISHED, user_names.size + 1)
      
      new_params = Hash.new(0) 
      new_params = { 
                  :user_from   => @userFB,
                  :user_to   => @userFB,
                  :main_message => scenario_themself,   
                  }
      feeds_to_publish.push new_params
      threads = []  
      threads << Thread.new do  
        
        
        begin 
          timeout 10 do 
            session_new.batch do 
              feeds_to_publish.each {|one|
                publishing(one[:user_from],one[:user_to],one[:main_message],Feed_action,FB_app_link + "/?f=#{FROM_FEEDS}",processing_phraze(@today_question.feedtext1, @user_local.first_name, self_publishing,@user_local.gender,user_names.size ) ,FB_app_link + "/?f=#{FROM_FEEDS}",processing_phraze(@today_question.feedtext2, @user_local.first_name, self_publishing,@user_local.gender,user_names.size ),'image',Current_site + @today_question.feedpic.url(:normal),FB_app_link + "/?f=#{FROM_FEEDS}")
              }
              #session batch
            end
            time_counter ("Session batch")
          end
        rescue TimeoutError 
          Rails.logger.warn " >" * 15
          Rails.logger.warn "     TIMEOUT FOR PUBLISHING "
          Rails.logger.warn " >" * 15
          render :action => :facebook_error
        end
      
        
      #thread end 
      end
      debug_message scenario_themself  
    # if user_has_published_once == true
    end
    return user_has_published_once
  end

  def greetings
    debug_message "Now session[:self_publishing] = '#{session[:self_publishing]}'"
    if params[:fanbutton] != nil
      redirect_to "http://www.facebook.com/pages/Chatting-and-Emailing-with-Nokia/10150115816675015"
    end

    if params[:mybutton] != nil
      p "params[:next_step] = #{params[:next_step]}"
      @question_number = params[:next_step]
      case @question_number 
        when "question1"
          today_question_select 0
        when "question2"
          today_question_select 1
        when "question3"
          today_question_select 2
      end

      render :action => @question_number  
    end 
  end

  def product
  end

  def fanpage
  end
  
  def facebook_error
  end
  
  def publishing(user_from,user_to,main_message,action_link_text,action_link_href,attachment_name,attachment_href,attachment_caption,media_type,media_src,media_href)
    user_from.publish_to(user_to,
        :message => main_message,
        :action_links => [
                {
                :text => action_link_text,
                :href => action_link_href}
                ],
        :attachment => {
                :name => attachment_name,
                :href => attachment_href,
                :caption => attachment_caption,

                :media => [
                        {
                          :type => media_type,
                          :src =>  media_src,
                          :href => media_href
                        }

                       ]})
  end
  
  def time_counter (object)
    the_time = Time.now - @time_counter
    @time_counter = Time.now
    p "#{object}: #{the_time.to_s}"
  end
end
