module UserHelper
  def tabs_styling(tab,current_action)
    html =""
    if (tab==current_action)
      html << " class='current' "
    end    
  end

  def fiveQuestions ()
    html = ""   
    for i in 1...6
      html << "#{i}. <fb:friend-selector  name='Some name for it#{i}' idname='friend_sel0#{i}' /><br><br>"
    end
    return html
  end

  def one_question (question)
    html  = ""  
    html << %{ 
    <h1 style="font-size:20px;">Nokia's E72 question of the day:</h1>
    <br>
    <center>
      <span style="font-size:14px;"> 
        #{question}
      </span>
    </center>
    <br>
    <br>
    <center>
      <% form_tag "question", {:id=>"testForm" ,:method => :put, :style => "font-size:14px;"}  do%>
         <%=fiveQuestions %>
        <%= submit_tag 'Submit answer to win Nokia E72' %>
      <%end%>
    </center>
    }
  end
end
